import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PostListComponentComponent} from './post-list-component/post-list-component.component';
import {NewPostComponent} from './new-post/new-post.component';

const appRoutes: Routes = [
  { path: 'posts', component: PostListComponentComponent },
  { path: 'new', component: NewPostComponent },
  { path: '', redirectTo: 'posts', pathMatch: 'full' },
  { path: '**', redirectTo: 'posts' }
];

  @NgModule({
    imports: [
      RouterModule.forRoot(
        appRoutes,
        {enableTracing: true}
      )
    ],
    exports: [RouterModule]
  })
  export class AppRoutingModule {}

