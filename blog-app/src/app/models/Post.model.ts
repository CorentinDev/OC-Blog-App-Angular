export class Post {
  constructor(
    public title: string,
    public content: string,
    public loveIt: number,
    public unLoveIt: number,
    public created_at: string
  ) {}
}
