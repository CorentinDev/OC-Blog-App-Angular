import {Component, Input, OnInit} from '@angular/core';
import { Post } from '../models/Post.model';
import {PostsService} from '../services/posts.service';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})
export class PostListItemComponentComponent implements OnInit {

  @Input() post: Post;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
  }

  onLoveIt() {
    this.post.loveIt = this.post.loveIt + 1;
    this.saveAndEmitPost();
  }

  onUnLoveIt() {
    this.post.unLoveIt = this.post.unLoveIt + 1;
    this.saveAndEmitPost();
  }

  onDelete(post: Post) {
    this.postsService.removePost(post);
  }

  saveAndEmitPost() {
    this.postsService.savePosts();
    this.postsService.emitPosts();
  }


}
