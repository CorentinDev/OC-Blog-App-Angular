import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor () {
    const config = {
      apiKey: "AIzaSyB09mgWSeLam2mTtdmWtF7mcyMuOGmuNrg",
      authDomain: "blog-appli.firebaseapp.com",
      databaseURL: "https://blog-appli.firebaseio.com",
      projectId: "blog-appli",
      storageBucket: "blog-appli.appspot.com",
      messagingSenderId: "975930757069"
    };
    firebase.initializeApp(config);
  }









}
