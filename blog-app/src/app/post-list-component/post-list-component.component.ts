import {Component, OnDestroy, OnInit} from '@angular/core';
import { Post } from '../models/Post.model';
import {Subscription} from 'rxjs/Subscription';
import {PostsService} from '../services/posts.service';


@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit, OnDestroy {

  listPost: Post[];
  postsSubscription: Subscription;



  constructor(private postsService: PostsService) { }

  ngOnInit() {

    this.postsSubscription = this.postsService.postsSubject.subscribe(
      (listPost: Post[]) => {
        this.listPost = this.sortListPost(listPost);
      }
    );
    this.postsService.emitPosts();
    this.postsService.getPosts();

  }


  ngOnDestroy() {
    this.postsSubscription.unsubscribe();
  }

  sortListPost(listPost: Post[]) {
    return listPost.sort(
      function(a, b) {
        return (a.created_at > b.created_at) ? -1 : ((b.created_at > a.created_at) ? 1 : 0);
      });
  }

}
